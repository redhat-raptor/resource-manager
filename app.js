var app = angular.module('resourceManagerApp', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/', {
            redirectTo: '/dashboard'
        }).
        when('/dashboard', {
            templateUrl: './partials/dashboard.html',
            controller: 'DashboardController'
        }).
        when('/not-available', {
            templateUrl: './partials/404.html',
            controller: 'NotAvailableController'
        }).
        otherwise({
            redirectTo: '/not-available'
        });
}]);
