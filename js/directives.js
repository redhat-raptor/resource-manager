app.directive('submissionForm', function() {
    return {
        restrict: 'E',
        template: '<div id="submissionForm" ng-bind-html="formHTML"></div>',
        replace: true,
        controller: function($scope, $element, $http, $sce, $routeParams, campaignService){
            if($routeParams.submissionId) {
                campaignService.findFormById($routeParams.submissionId).then(function(data) {
                        $scope.formHTML = $sce.trustAsHtml(data);
                    },
                    function(errorMessage) {
                        $scope.success = '';
                        $scope.errors = errorMessage;
                        console.warn(errorMessage);
                    }
                );
            }

            $($element).on('submit', function(e) {
                var r = confirm("Are you sure want to modify this submission?");
                if (r == true) {
                    e.preventDefault();
                    campaignService.saveFormData($('#submissionForm form')).then(
                        function(data) {
                            if(typeof data == 'string') {
                                $scope.success = '';
                                $scope.errors = 'Form validation failed!';
                                $scope.formHTML = $sce.trustAsHtml(data);
                            }
                            else {
                                $scope.errors = '';
                                $scope.success = 'Saved!';
                                $scope.formHTML = $sce.trustAsHtml(data.form);
                                $scope.submission = data.submission;

                                $('#form_admin_note').val('');
                            }

                            $scope.refreshLogs();
                            $('html, body').animate({scrollTop:0}, 'slow');
                        },
                        function(errorMessage) {
                            $scope.success = '';
                            $scope.errors = errorMessage;
                            console.warn(errorMessage);
                            $scope.refreshLogs();
                            $('html, body').animate({scrollTop:0}, 'slow');
                        }
                    );
                } else {
                    return false;
                }
            });
        },
        link: function(scope, element, attr) {
        }
    }
});