var x2js = new X2JS({
    emptyNodeForm : "object",
    arrayAccessForm : "property",
    skipEmptyTextNodesForObj : false
});

function DoubleList(type) {
    this.type = type;
    this.unselItems = [];
    this.selItems = [];

    this.addSelItem = function(item) {
        if(this.selItems.indexOf(item) !== -1) return;

        this.selItems.push(item);
        this.removeUnselItem(item);
    }

    this.addUnselItem = function(item) {
        if(this.unselItems.indexOf(item) !== -1) return;

        this.unselItems.push(item);
        this.removeSelItem(item);
    }

    this.removeUnselItem = function(item) {
        this.unselItems = _.without(this.unselItems, item);
    }

    this.removeSelItem = function(item) {
        this.selItems = _.without(this.selItems, item);
    }

    this.getUnselItems = function() {
        return this.unselItems;
    }

    this.getSelItems = function() {
        return this.selItems;
    }
}

var DragDropManager = function(draggableItem, droppableItem, callback)  {
    $(draggableItem).draggable({
        revert: "invalid",
        containment: "document",
        helper: "clone",
        cursor: "move"
    });

    $(droppableItem).droppable({
        accept: draggableItem,
        activeClass: "ui-state-hover",
        hoverClass: "ui-state-active",
        drop: function(event, ui) {
            ui.draggable.appendTo(this);
            callback(ui.draggable);
        }
    });
}

var app = {
    managerList: new DoubleList('Manager'),
    init: function(unselectedManagers, selectedManagers) {
        var that = this;

        _.each(unselectedManagers, function(item) {
            that.managerList.addUnselItem(item);
        });

        _.each(selectedManagers, function(item) {
            that.managerList.addSelItem(item);
        });        
        
        //Show unselected items
        var tplContent = this.getListHTML(this.managerList.getUnselItems());
        $('#unsel-managers').append(tplContent);

        //Show already selected items
        var tplContent = this.getListHTML(this.managerList.getSelItems());
        $('#sel-managers').append(tplContent);

        var managerSelDragDrop = new DragDropManager(
            $('#unsel-managers li, #sel-managers li'), 
            $('#sel-managers, #unsel-managers'), 
            function(draggable) {
                if(draggable.parent().is('#sel-managers')) {
                    that.managerList.addSelItem(draggable.text());
                }
                else {
                    that.managerList.addUnselItem(draggable.text());
                }

                that.render(that.managerList);
            }
        );

        this.render(this.managerList);
    },
    getListHTML: function(list) {
        var listTemplate = $("#item-list-template").html();
        var tpl = _.template(listTemplate);
        return tpl({list: list});
    },
    render: function(doubleList) {
        var xmlDocStr = x2js.json2xml_str({collection: this.managerList});
        $('#xmlContent').html(escapeHtml(xmlDocStr));
    }
}

$(function() {
    "use strict";

    var xmlText = '<managers><unselected><person name="Humayun"/><person name="Ahsanul"/><person name="Ziaul"/><person name="Rezaul"/></unselected><selected><person name="Shirin"/></selected></managers>';
    var jsonObj = x2js.xml_str2json(xmlText);

    var unselectedManagers = [];
    _.each(jsonObj.managers.unselected.person, function(item) {
        var name = typeof item == 'string' ? item : item._name;
        unselectedManagers.push(name);
    });

    var selectedManagers = [];
    _.each(jsonObj.managers.selected.person, function(item) {
        var name = typeof item == 'string' ? item : item._name;
        selectedManagers.push(name);
    });

    //Bootstrap app
    app.init(unselectedManagers, selectedManagers);

    $(".list-group li").disableSelection();
});


function escapeHtml(string) {
    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };

    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}