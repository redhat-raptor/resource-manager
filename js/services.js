app.service("resourceService", function($http, $q) {
        // Public API.
        return({
            getResources: getResources,
            getUsers: getUsers,
            getRoles: getRoles,
            getTaskSkel: getTaskSkel,
            getDepartmentSkel: getDepartmentSkel
        });

        function getTaskSkel() {
            return {
                user_id: null,
                user_type: null,
                role_id: null,
                discount_from: 'customer_rate',
                discount: null,
                allocation: null
            }
        }

        function getDepartmentSkel(name) {
            return {
                name: name,
                tasks: [{
                    resources: [getTaskSkel()]
                }]
            }
        }

        function getResources() {
            return {
                project: {
                    name: 'CUE Project',
                    departments: [
                        {
                            name: 'Design',
                            tasks: [
                                {
                                    resources: [
                                        {
                                            user_id: 4,
                                            user_type: 'resource_pool',
                                            role_id: 1,
                                            discount_from: 'customer_rate',
                                            discount: 5,
                                            allocation: 10
                                        }
                                    ]
                                },
                                {
                                    resources: [{
                                        user_id: 1,
                                        user_type: 'user',
                                        role_id: 2,
                                        discount_from: 'customer_rate',
                                        discount: 3,
                                        hours: 40
                                    }]
                                }
                            ]
                        },
                        {
                            name: 'Development',
                            tasks: [
                                {
                                    resources: [{
                                        user_id: 2,
                                        user_type: 'user',
                                        role_id: 2,
                                        discount_from: 'customer_rate',
                                        discount: '',
                                        allocation: 20
                                    }]
                                }
                            ]
                        },
                        {
                            name: 'Quality controller',
                            tasks: []
                        }
                    ]
                }
            };
            /*var request = $http({
                method: "get",
                url: "/api/submissions/callop",
                params: {
                    cache: new Date().getTime()
                }
            });

            return(request.then(handleSuccess, handleError));*/
        }

        function getUsers() {
            return {
                user: {
                    name: 'User',
                    list: [
                        {id: 1, name: 'Humayun'},
                        {id: 2, name: 'Rezaul'},
                        {id: 3, name: 'Ziaul'}
                    ]
                },
                resource_pool: {
                    name: 'Resource Pool',
                    list: [
                        {id: 4, name: 'Shirin'},
                        {id: 5, name: 'Babu'}
                    ]
                }
            };
        }

        function getRoles() {
            return [
                {
                    id: 1,
                    name: 'VP Marketing',
                    customer_rate: 300,
                    standard_rate: 750
                },
                {
                    id: 2,
                    name: 'VP Technology',
                    customer_rate: 350,
                    standard_rate: 800
                }
            ];
        }

        // Private methods
        function handleError(response) {
            if (!angular.isObject( response.data ) || !response.data.message) {
                return($q.reject("An unknown error occurred. Server response code: " + response.status));
            }

            // Otherwise, expected error message.
            return($q.reject(response.data.message));

        }

        function handleSuccess(response) {
            return(response.data);
        }
    }
);
