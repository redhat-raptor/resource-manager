app.controller('DashboardController', ['$scope', '$window', '$http', '$routeParams', '$rootScope', 'resourceService', function ($scope, $window, $http, $routeParams, $rootScope, resourceService) {
    function init() {
        $scope.resourceData = resourceService.getResources();

        angular.forEach($scope.resourceData.project.departments, function(department, index) {
            if(!department.tasks) department.tasks = [];
            department.tasks.push({});

            angular.forEach(department.tasks, function(task, index) {
                //Every task should have one empty resource by default
                if(!task.resources || task.resources.length == 0) {
                    task.resources = [];
                    task.resources.push(resourceService.getTaskSkel());
                }
            });
        });

        $scope.users = resourceService.getUsers();
        $scope.roles = resourceService.getRoles();

        console.log($scope.resourceData);
        console.log($scope.users);
    };

    $scope.getCustomerRate = function(task) {
        var role = _.findWhere($scope.roles, {id: task.role_id});
        return role ? role.customer_rate : '';
        
    }

    $scope.getStandardRate = function(task) {
        var role = _.findWhere($scope.roles, {id: task.role_id});
        return role ? role.standard_rate : null;
    }

    $scope.getSellRate = function(task) {
        if(!task.discount_from) return;

        var role = _.findWhere($scope.roles, {id: task.role_id});
        if(!role) return;

        var amount = role[task.discount_from];
        if(!amount) return;

        return amount - ((amount * task.discount) / 100);
    }

    $scope.addTask = function(department) {
        department.tasks.push(resourceService.getTaskSkel());
    }

    $scope.addResource = function(task) {
        task.resources.push(resourceService.getTaskSkel());
    }

    $scope.removeResource = function(task, resource, index) {
        if(confirm('Confirm remove?')) task.resources.splice(index, 1);
    }

    $scope.addDepartment = function() {
        if(_.findWhere($scope.resourceData.project.departments, {name: $scope.newDepartmentName})) return;
        $scope.resourceData.project.departments.push(resourceService.getDepartmentSkel($scope.newDepartmentName));

        $scope.newDepartmentName = '';
    }

    init();
}]);

app.controller('NotAvailableController', ['$http', function($scope, $http) {
    function init() {};
    init();
}]);